package ru.anenkov.tm.entity;

public class Session extends AbstractEntity implements Cloneable {

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    private Long timestamp;

    private String userId;

    private String signature;

    public Session() {
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "\nSession:\n" +
                "\ntimestamp =" + timestamp +
                ", \nuserId ='" + userId + '\'' +
                ", \nsignature ='" + signature + '\'' +
                "\n";
    }

}
