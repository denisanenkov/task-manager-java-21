package ru.anenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.ISessionRepository;
import ru.anenkov.tm.api.service.IPropertyService;
import ru.anenkov.tm.api.service.ISessionService;
import ru.anenkov.tm.api.service.IUserService;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.empty.EmptySignatureSessionException;
import ru.anenkov.tm.exception.empty.EmptyTimestampSessionException;
import ru.anenkov.tm.exception.empty.EmptyUserIdSessionException;
import ru.anenkov.tm.exception.user.AccessDeniedException;
import ru.anenkov.tm.util.HashUtil;
import ru.anenkov.tm.util.SignatureUtil;

import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    ISessionRepository sessionRepository;

    IUserService userService;

    IPropertyService propertyService;

    public SessionService(
            @NotNull final ISessionRepository sessionRepository,
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService
    ) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public boolean checkDataAccess(String login, String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) return false;
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public Session open(String login, String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) return null;
        @NotNull Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.merge(session);
        return sign(session);
    }

    @Override
    public Session sign(Session session) {
        if (session == null) return null;
        session.setSignature(null);
        final String salt = "abracadabra";
        final Integer cycle = 4440;
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public User getUser(Session session) {
        final String userId = getUserId(session);
        return userService.findById(userId);
    }

    @Override
    public String getUserId(Session session) {
        validate(session);
        return session.getUserId();
    }

    @Override
    public List<Session> getListSession(Session session) {
        validate(session);
        return sessionRepository.findByUserId(session.getUserId());
    }

    @Override
    public void close(Session session) {
        validate(session);
        sessionRepository.remove(session);
    }

    @Override
    public void closeAll(Session session) {
        validate(session);
        sessionRepository.removeByUserId(session.getUserId());
    }

    @Override
    public void validate(Session session, Role role) {
        if (role == null) return;
        validate(session);
        final String userId = session.getUserId();
        final User user = userService.findById(userId);
        if (user == null) return;
        if (user.getRole() == null) return;
        if (!role.equals((user.getRole()))) return;
    }

    @Override
    public void validate(Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty())
            throw new EmptySignatureSessionException();
        if (session.getUserId() == null || session.getUserId().isEmpty())
            throw new EmptyUserIdSessionException();
        if (session.getTimestamp() == null)
            throw new EmptyTimestampSessionException();
        final Session temp = session.clone();
        if (temp == null) return;
        final String signatureSource = session.getSignature();
        final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) return;
    }

    @Override
    public void signOutByLogin(String login) {
        if (login == null || login.isEmpty()) return;
        final User user = userService.findByLogin(login);
        if (user == null) return;
        final String userId = user.getId();
        sessionRepository.removeByUserId(userId);
    }

    public void remove(Session session) {
        sessionRepository.remove(session);
    }

    @Override
    public void signOutByUserId(String userId) {
        if (userId == null || userId.isEmpty()) return;
        sessionRepository.removeByUserId(userId);
    }

    @Override
    public boolean isValid(Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
