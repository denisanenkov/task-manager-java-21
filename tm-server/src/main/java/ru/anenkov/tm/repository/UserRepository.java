package ru.anenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.IUserRepository;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.user.AccessDeniedException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final List<User> users = new ArrayList<>();

    @Nullable
    @Override
    public List<User> findAll() {
        return users;
    }

    @Nullable
    @Override
    public User add(final @NotNull User user) {
        users.add(user);
        return user;
    }

    @Nullable
    @Override
    public User findById(final @NotNull String id) {
        for (@NotNull final User user : users) {
            if (user.getId().equals(id)) {
                return user;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public User findByLogin(final @NotNull String login) {
        for (@NotNull final User user : users) {
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public User removeById(final @NotNull String id) {
        @Nullable final User user = findById(id);
        if (user == null) return null;
        removeUser(user);
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(final @NotNull String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        removeUser(user);
        return user;
    }

    @Nullable
    @Override
    public User removeUser(final @NotNull User user) {
        users.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User findByEmail(final @NotNull String email) {
        for (@NotNull User user : users) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User removeByEmail(final @NotNull String email) {
        @Nullable final User user = findByEmail(email);
        if (user == null) return null;
        users.remove(user);
        return removeUser(user);
    }

    @Override
    public void clear() {
        users.clear();
    }

    @Override
    public void merge(final @Nullable Collection<User> users) {
        if (users == null) return;
        for (@NotNull final User user : users) merge(user);
    }

    @Override
    public @Nullable void merge(final @Nullable User user) {
        if (user == null) return;
        users.add(user);
    }

    @Override
    public void merge(@NotNull final User... users) {
        if (users == null) return;
        for (@NotNull final User user : users) merge(user);
    }

    @Override
    public void load(final Collection<User> users) {
        clear();
        merge(users);
    }

    @Override
    public void load(@NotNull final User... users) {
        clear();
        merge(users);
    }

    @Override
    public void load(@NotNull final User user) {
        clear();
        merge(user);
    }

    @Nullable
    @Override
    public List<User> getList() {
        return users;
    }

}
