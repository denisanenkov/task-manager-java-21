package ru.anenkov.tm.endpoint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.endpoint.ISessionEndpoint;
import ru.anenkov.tm.api.service.IServiceLocator;
import ru.anenkov.tm.api.service.ISessionService;
import ru.anenkov.tm.dto.Fail;
import ru.anenkov.tm.dto.Result;
import ru.anenkov.tm.dto.Success;
import ru.anenkov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.validation.constraints.NotNull;
import java.util.List;

@WebService
@JsonIgnoreProperties
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public SessionEndpoint() {
        super(null);
    }

    @WebMethod
    @Override
    @Nullable
    public Session openSession(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {
        final ISessionService sessionService = super.serviceLocator.getSessionService();
        return sessionService.open(login, password);
    }

    @WebMethod
    @Override
    @Nullable
    public Result closeSession(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        super.serviceLocator.getSessionService().validate(session);
        try {
            super.serviceLocator.getSessionService().close(session);
            return new Success();
        } catch (final RuntimeException e) {
            return new Fail();
        }
    }

    @WebMethod
    @Override
    @Nullable
    public Result closeSessionAll(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        super.serviceLocator.getSessionService().validate(session);
        try {
            super.serviceLocator.getSessionService().closeAll(session);
            return new Success();
        } catch (final RuntimeException e) {
            return new Fail();
        }
    }

    @WebMethod
    @Override
    @Nullable
    public List<Session> allSessions(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        super.serviceLocator.getSessionService().validate(session);
        List<Session> sessions = super.serviceLocator.getSessionService().getListSession(session);
        System.out.println(sessions);
        return sessions;
    }

}
