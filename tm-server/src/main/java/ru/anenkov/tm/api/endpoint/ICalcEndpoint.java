package ru.anenkov.tm.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ICalcEndpoint {

    @WebMethod
    int sum(
            @WebParam (name = "a") int a,
            @WebParam (name = "b") int b
    );

}
