package ru.anenkov.tm.api.repository;

import ru.anenkov.tm.entity.Session;
import org.jetbrains.annotations.Nullable;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    void removeByUserId(@NotNull String userId);

    @Nullable
    List<Session> findByUserId(@NotNull String userId);

}
