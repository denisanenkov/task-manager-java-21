package ru.anenkov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.Result;
import ru.anenkov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.validation.constraints.NotNull;
import java.util.List;

@WebService
public interface ISessionEndpoint {

    @NotNull
    @WebMethod
    Session openSession(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    );

    @NotNull
    @WebMethod
    Result closeSession(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @NotNull
    @WebMethod
    Result closeSessionAll(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @NotNull
    @WebMethod
    List<Session> allSessions(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

}
