package ru.anenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class UserUpdateMiddleNameClientCommand extends AbstractCommandClient {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Update-second-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Update second name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER MIDDLE NAME]");
        System.out.println("ENTER NEW USER MIDDLE NAME: ");
        @NotNull final String newSecondName = TerminalUtil.nextLine();
        bootstrap.getUserEndpoint().updateUserMiddleName(bootstrap.getSession(), newSecondName);
        System.out.println("[NAME UPDATE SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
