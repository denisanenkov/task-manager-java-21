package ru.anenkov.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;

import java.io.IOException;

public class DataBinarySaveCommand extends AbstractCommandClient {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Data-bin-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA BINARY SAVE]");
        bootstrap.getAdminEndpoint().saveDataBinary(bootstrap.getSession());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
