package ru.anenkov.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;

public class DataJsonClearCommand extends AbstractCommandClient {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Data-json-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear data from json file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON CLEAR]");
        bootstrap.getAdminEndpoint().cleanDataJson(bootstrap.getSession());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
