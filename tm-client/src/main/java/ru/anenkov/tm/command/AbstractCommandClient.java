package ru.anenkov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.bootstrap.BootstrapClient;
import ru.anenkov.tm.enumeration.Role;

public abstract class AbstractCommandClient {

    @Nullable
    protected BootstrapClient bootstrap;

    public void setServiceLocator(@Nullable final BootstrapClient bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void execute() throws Exception;

}
