package ru.anenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.Project;
import ru.anenkov.tm.endpoint.Task;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class ProjectFindByIdClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Show-project-by-id";
    }

    @Override
    public @Nullable String description() {
        return "Show project by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Project project = bootstrap.getProjectEndpoint().findOneByIdProject(bootstrap.getSession(), id);
        if (project == null) return;
        System.out.println("" +
                "NAME: " + project.getName() +
                ", \nDESCRIPTION: " + project.getDescription() +
                ", \nUSER ID: " + project.getUserId() +
                ", \nTASK ID: " + project.getId()
        );
        System.out.println("[SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
