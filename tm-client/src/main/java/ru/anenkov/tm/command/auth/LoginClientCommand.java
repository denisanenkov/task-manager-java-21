package ru.anenkov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.Session;
import ru.anenkov.tm.util.TerminalUtil;

public class LoginClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return "-log";
    }

    @Override
    public @Nullable String name() {
        return "Login";
    }

    @Override
    public @Nullable String description() {
        return "Login user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGIN]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final Session session = bootstrap.getSessionEndpoint().openSession(login, password);
        bootstrap.setSession(session);
        if (bootstrap.getSession() != null)
        System.out.println("[LOGIN SUCCESS]");
    }

}
