package ru.anenkov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;

import java.sql.SQLOutput;

public class LogoutClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return "-lout";
    }

    @Override
    public @Nullable String name() {
        return "Logout";
    }

    @Override
    public @Nullable String description() {
        return "Logout user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGOUT]");
        this.bootstrap.getSessionEndpoint().closeSession(bootstrap.getSession());
        this.bootstrap.clearSession();
        System.out.println("[LOGOUT SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
